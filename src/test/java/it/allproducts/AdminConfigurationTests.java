package it.allproducts;

import com.edwardawebb.atlassian.statuspage.impl.ConfigResource;
import it.AbstractTest;
import org.apache.wink.client.ClientResponse;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test component imports and exports of the test plugin when actually installed in the product.
 */
public class AdminConfigurationTests extends AbstractTest
{


    @Test
    public void testConfigIsConfigable()
    {
        enableStatusPage(COMPONENTS,PAGE_ID);
        //operation above returns empty payload, GET updated config to verify
        ClientResponse response = client.resource(REST_URL).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
        adminConfig = response.getEntity(ConfigResource.Config.class);
        assertTrue("Page should be enabled but is not", adminConfig.isEnabled());
        assertEquals("Page ID does not match provided",adminConfig.getPageId(),PAGE_ID);
        assertEquals("Expected component did not match provided",adminConfig.getComponents(), COMPONENTS);
    }


}