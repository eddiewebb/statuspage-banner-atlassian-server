// methods below will augment StatusPage objects with additional details based on these maps
var icons = {'scheduled':'time','in_progress':'warning','identified':'flag','investigating':'view','monitoring':'review','resolved':'approve'};
var colors = {'maintenance':'#f1c40f','critical':'#e74c3c','major':'#e67e22','minor':'#f1c40f','none':'#2ecc71'} ;


function augmentIncidents(summary){
   var progressStatusWeights = {
        'investigating':{weight:0},
        'identified':{weight:1},
        'monitoring':{weight:2},
        'resolved':{weight:3}
        };
    var worstProgressStatus = "resolved";
    var i = summary.incidents.length;
    while(i--){
        var incident = summary.incidents[i];
        summary.incidents[i].statusIcon = icons[incident.status];
        summary.incidents[i].impactColor = colors[incident.impact];
        summary.incidents[i].incident_updates[0].statusIcon = icons[incident.incident_updates[0].status];
        if(statuspageConfig.configuredComponents){
            var keepIncident = false;
            var j = incident.components.length;
            while(j--){
                var component = incident.components[j];
                if( !statuspageConfig.configuredComponents || statuspageConfig.configuredComponents.indexOf(component.name) >= 0 ){
                    // No filter specified, or we care sbout this one, keep this compomnent
                    keepIncident = true;
                    break;//leave components, we'll keep it
                }
            }
            if( ! keepIncident ){
                 //no matching components, drop it!
                 summary.incidents.splice(i,1);
                 continue; // next incident, this ones rubbish
             }
        }
        if( progressStatusWeights[incident.status].weight < progressStatusWeights[worstProgressStatus].weight ){
            console.log("Lowering progress from " + worstProgressStatus +" to " + incident.status + " based on incident: " + incident.name);
            worstProgressStatus = incident.status;
        }
    }
    summary.status.progress = worstProgressStatus;
    return summary;
}

function augmentMaintenances(summary){
    var i = summary.scheduled_maintenances.length;
    while(i--){
        var incident = summary.scheduled_maintenances[i];
        summary.scheduled_maintenances[i].statusIcon = icons[incident.status];
        summary.scheduled_maintenances[i].impactColor = colors[incident.impact];
        if(statuspageConfig.configuredComponents){
            var keepIncident = false;
            var j = incident.components.length;
            while(j--){
                var component = incident.components[j];
                if( !statuspageConfig.configuredComponents || statuspageConfig.configuredComponents.indexOf(component.name) >= 0 ){
                    // we care, keep this compomnent
                    keepIncident = true;
                    break; //checking components
                }
            }
            if( ! keepIncident ){
                //no matching components, drop it!
                summary.scheduled_maintenances.splice(i,1);
                continue; //looping incidents
            }
            // any code to apply to kept modules..

        }
    }
    return summary;
}

function filterByComponents(summary){
    if(statuspageConfig.configuredComponents){
        //we need to capture 'the worse' status and description across components, this is the weightimng used to determine
        // We filter specific incidents below, but in case the operator forgets to associate a component to an incident this gives us most accurat cuccrent status
        var impactStatusMappings = {
                    'operational':{impact:'none',weight:0,description:'All Systems Operational'},
                    'under_maintenance':{impact:'maintenance',weight:1,description:'Service Under Maintenance'},
                    'degraded_performance':{impact:'minor',weight:2,description:'Partially Degraded Service'},
                    'partial_outage':{impact:'minor',weight:3,description:'Minor Service Outage'},
                    'major_outage':{impact:'major',weight:4,description:'Major Service Outage'}
                    };
        var highestStatus = "operational"; //default to lowest
        var i = summary.components.length;
        while(i--){
            var component = summary.components[i];
            if( statuspageConfig.configuredComponents.indexOf(component.name) >= 0 ){
                //is a component we care about, see if it changes status
                if(impactStatusMappings[component.status].weight > impactStatusMappings[highestStatus].weight ){
                    highestStatus = component.status;
                }
            }else{
                //not a component we care about, drop it!
                summary.components.splice(i,1);
            }
        }
        // ignore global SP status, we'll update it below.
        summary.status.indicator = impactStatusMappings[highestStatus].impact;
        summary.status.description = impactStatusMappings[highestStatus].description;
    }
    summary.status.color = colors[summary.status.indicator];
    return summary;
}


function modifyStatusPageJs(summary){
    summary = filterByComponents(summary);
    summary = augmentIncidents(summary);
    summary = augmentMaintenances(summary);
    console.log({"Message":"Updated SP Object"},summary);
    return summary;
}

