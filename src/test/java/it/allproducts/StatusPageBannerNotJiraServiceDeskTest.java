package it.allproducts;

import it.AbstractTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test that requisites for our plugin are loaded in the product. This does not validate functionality of our pieces, just that they are all present.
 */
public class StatusPageBannerNotJiraServiceDeskTest extends AbstractTest
{


    @Before
    public void setup() throws MalformedURLException {
        createDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }


    /**
     * This only tests that products expose the parent {@code <div>} we append to.
     * It does not validate JS execution or functionality
     * @throws IOException
     */
    @Test
    public void testParentElementsArePresentInDom() throws IOException {

        enableStatusPage(null,PAGE_ID);
        driver.get(BASE_URL);
        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='aui-header-primary']/ul[@class='aui-nav']"));
        assertThat("Unable to find SP elements" ,elements.size(), not(0));
        driver.executeScript("sauce:job-result=passed");
    }
    /**
     * This tests that product exposes a working AJS required by our js code.
     * @throws IOException
     */
    @Test
    public void testAjsIsAvailable() throws IOException, InterruptedException {

        enableStatusPage(null,PAGE_ID);
        driver.get(BASE_URL);
        Object ajsResult = driver.executeScript("return AJS.$.length;");
        String asString = ajsResult.toString();
        assertTrue("Unable to reference AJS" ,Integer.parseInt(asString) > 0);
        driver.executeScript("sauce:job-result=passed");
    }

    /**
     * This tests that product exposes a working SOy (google closures) required by our js code.
     * @throws IOException
     */
    @Test
    public void testBannerRenders() throws IOException, InterruptedException {
        enableStatusPage(null,MOCK_PAGE_ID);
        WebDriverWait wait = new WebDriverWait(driver,5);
        driver.get(BASE_URL);
        //wait for dynamic status from sp.io test page
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("statuspage-summary-menu-item")));
        analyzeLog(driver);
        String statusSummary = driver.findElement(By.id("statuspage-summary-menu-item")).getText();
        assertThat(statusSummary,is(MOCK_SP_STATUS));
        driver.executeScript("sauce:job-result=passed");
    }

    public void analyzeLog(WebDriver driver) {
        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
            System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
        }
    }



}